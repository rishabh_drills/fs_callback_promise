const fs = require("fs")

function createFolder (){
     
        return new Promise((resolve, reject) => {
            let dir = "newFolder";
            if(fs.existsSync(dir)){
                console.log("Directory exists!")
            }else{
                console.log("Directory not found")
            }
            fs.mkdir("newFolder", (err) =>{
                if(!err){
                    resolve()
                }else{
                    reject()
                }
            })
         })
           
    
}

function newFile(){
    return new Promise((resolve,reject) =>{
        for(let i=0;i<5;i++){
            fs.writeFile(`something${i}.json`, "Hello World", (err) =>{
                if(!err){
                    resolve("file created")
                }else{
                    reject("file")
                }
            })
        }
    })
}

function deleteFile(){
    return new Promise((resolve, reject) =>{
        for(let i=0;i<5;i++){
            fs.unlink(`something${i}.json`, (err) => {
                if(!err){
                    resolve()
                }else{
                    reject("not")
                }
            })
        }
    })
}

/* newFile()
.then((message) => {
    console.log("File has been created" + message)
    return deleteFile()
}).then(() =>{
    console.log("deleted the file")
}).catch((message)=>{
    console.log("Error catched" + message)
}) */

createFolder()
.then(() => {
    console.log("Folder created")
    return newFile()
}).then(() => {
    console.log("created files")
    return deleteFile()
}).then(() => {
    console.log("deleted files")
}).catch(() => {
    console.log("error found")
})