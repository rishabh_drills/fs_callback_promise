const fs = require("fs")

function readFile(){
    return new Promise((resolve, reject) => {
        fs.readFile("./lipsum.txt", "utf-8", (err, data) => {
            if(!err){
                resolve(data)
            }else{
                reject("Error")
            }
        })
    })
}


function convertAndCreateNewFile(data){
    return new Promise((resolve, reject) => {
        let upper = data.toUpperCase()
        // console.log(upper)
        fs.writeFile("./newFile1.txt", upper, (err) => {
            if(!err){
                resolve("converte to upper")
            }else{
                reject("Error")
            }
        })
     })
}


function storeFileNameInFileNames(data){
    return new Promise((resolve, reject) => {
        fs.writeFile("./fileNames.txt", "newFile1.txt", (err)=>{
            if(!err){
                resolve(data)
            }else{
                reject("Unable to add")
            }
        
    })
  })
    

}


function readAndConvertToLowerCase(){
    return new Promise((resolve, reject) => {
        fs.readFile("./newFile1.txt", "utf-8", (err, UpperData) =>{
            if(!err){
                resolve(UpperData)
            }else{
                reject();
            }
            let loweredData = UpperData.toLowerCase();
            let splitedData = loweredData.split(".")
            
            let file = fs.createWriteStream("newFile2.txt")
            file.on("error", (err) =>{
                console.error(err);
            })
            splitedData.forEach(element => file.write(`${element}\r\n`))
            file.end();
            fs.appendFile("./fileNames.txt", " newFile2.txt", (err)=>{
                if(err){
                    console.error(err)
                }else{
                    console.log("newfile")
                }
            })
        })
    })
}


function sortingContent(data){
    return new Promise((resolve, reject) => {
        fs.readFile("./newFile2.txt", "utf-8", (err, sortedArr)=>{
            if(!err){
                resolve(sortedArr);
            }else{
                reject(err)
            }
            sortedArr = Array.of(sortedArr);
            let sortedArray = sortedArr.sort();
            sortedArray = sortedArray.toString();
            fs.writeFile("newFile3.txt", sortedArray, (err)=>{
                if(err){
                    console.error(err)
                }else{
                    console.log("sorted")
                }
            })
            fs.appendFile("./fileNames.txt", " newFile3.txt", (err) => {
                if(err){
                    console.error(err)
                }
            })
        })
    })
}


function readFilesName(data){
    return new Promise((resolve, reject) =>{
      fs.readFile("./fileNames.txt", "utf-8", (err, data) =>{
          if(!err){
              resolve(data)
          }else{
              reject()
          }
      })  
    })
}

function deleteFiles(data){
    return new Promise((resolve, reject) => {
        let splitFilesToDelete = data.split(" ");
        let deletedFiles = splitFilesToDelete.map((val) => {
        fs.unlink(val, function(err){
        if(!err){
            resolve();
        }else{
            reject()
             }
          })
        })
    })
}

readFile()
.then((data) => {
    console.log("The file is created & converted to upper case ")
    return convertAndCreateNewFile(data)
})
.then((data) =>{
    console.log("store file")
    return storeFileNameInFileNames(data)
})
.then((data) => {
    console.log("Converted file to lowerCase and made new one ")
    return readAndConvertToLowerCase(data)
})
.then((data) =>{
    console.log("file 3 made and added")
    return sortingContent(data)
})
.then((data) => {
    console.log("The fileNames has been read")
    return readFilesName(data)
})
.then((data) => {
    console.log("Files delete")
    return deleteFiles(data)
})
.catch(() =>{
    console.log("Error")
})